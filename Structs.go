package main

//Channel is the struct for a twitch channel config
type Channel struct {
	Commands      []Command       `json:"commands"`
	CommandPrefix string          `json:"commandPrefix"`
	Lists         map[string]List `json:"lists"`
	Quotes        []Quote         `json:"quotes"`
	RollLevel     string          `json:"rollLevel"`
}

//A Command represents a triggered response for a keyword
type Command struct {
	Editor string `json:"editor"`
	Count  int    `json:"count"`
	Value  string `json:"value"`
	Key    string `json:"key"`
}

type List struct {
	Restriction int      `json:"restriction"`
	Items       []string `json:"items"`
}

type Quote struct {
	Timestamp int64  `json:"timestamp"`
	Editor    string `json:"editor"`
	Text      string `json:"quote"`
}

type Config struct {
	Admin        string              `json:"admin"`
	BotID        string              `json:"bot_id"`
	WebsiteRoot  string              `json:"website_root"`
	Token        string              `json:"token"`
	Channels     map[string]struct{} `json:"channels"`
	Associations map[string]string   `json:"associations"`
}
