package main

import (
	"encoding/json"
	"fmt"
	"github.com/bwmarrin/discordgo"
	//spew is used for struct debugging
	//"github.com/davecgh/go-spew/spew"
	"io/ioutil"
	"math/rand"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"
)

//declare the global variables for the local config,  and which channel configs we are using from coebot.tv
var (
	config   Config
	channels map[string]Channel
)

func main() {
	//initialize the channel map
	channels = make(map[string]Channel)

	//load the local config and download any channel configs that we need
	loadConfig()

	//connect to discord
	dg, errors := discordgo.New("Bot " + config.Token)

	if errors != nil {
		fmt.Printf("error", errors)
		return
	}

	//add callbacks for new messages, connections and disconnections
	dg.AddHandler(messageCreate)
	dg.AddHandler(onConnect)
	dg.AddHandler(onDisconnect)

	errors = dg.Open()
	if errors != nil {
		fmt.Println("error opening connection,", errors)
		return
	}

	//keep the bot running
	//TODO change how this works lol
	fmt.Println("Ctrl + c to exit")
	<-make(chan struct{})

	return

}

//implement better reconnects
func onDisconnect(s *discordgo.Session, d *discordgo.Disconnect) {

}

//load the local config, which includes information like login info, and guild-to-twitchchannel associations
func loadConfig() {
	fmt.Println("Loading config")

	config = Config{}
	configFile, _ := ioutil.ReadFile("config.json")

	if err := json.Unmarshal(configFile, &config); err != nil {
		panic(err)
	}

	//iterate over the map of channels, and download the channels from the website
	for key := range config.Channels {
		downloadConfig(key)
	}

}

//downloads the channel configs from the website and stores them in the channel map
func downloadConfig(channel string) {
	tempChan := Channel{}
	fmt.Println(config.WebsiteRoot + "configs/" + channel + ".json")
	getJson(config.WebsiteRoot+"configs/"+channel+".json", &tempChan)
	channels[channel] = tempChan

}

//associate a discord Guild with a twitch channel name
func addAssociation(server, channel string) {
	_, present := config.Associations[server]
	if present {
		return
	}
	config.Associations[server] = channel
	config.Channels[channel] = struct{}{}
	downloadConfig(channel)
	saveConfig()
}

//download json from url
func getJson(url string, target interface{}) error {
	r, err := http.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

//writes the current config to file
func saveConfig() {

	configjson, _ := json.Marshal(config)
	err := ioutil.WriteFile("config.json", configjson, 0644)
	check(err)

}

//checks for any errors and logs them
func check(e error) {
	if e != nil {
		fmt.Printf("error", e)
	}
}

//callback for connection to discord
func onConnect(S *discordgo.Session, c *discordgo.Connect) {

	fmt.Printf("\nConnected to discord.\n")
}

//callback for a new message in a channel the bot is in
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	//ignore messages from the bot itself so it doesnt loop
	if m.Author.ID == config.BotID {
		return
	}

	//get some variables out of the message data for ease of use
	channel, _ := s.Channel(m.ChannelID)
	serverID := channel.GuildID
	server, _ := s.Guild(serverID)
	serverOwner := server.OwnerID

	fmt.Printf("%20s %20s %20s > %s\n", server.Name+":"+channel.Name, time.Now().Format(time.Stamp), m.Author.Username, m.Content)

	mentions := m.Mentions

	//if the message has a mention in it, and if it's us, we'll check if it's to set the channel association
	//we cant use the prefix of the channel for this because if the discord server owner isn't partnered,
	//no channel information will be downloaded yet
	if len(mentions) > 0 && mentions[0].ID == config.BotID {
		message := strings.TrimPrefix(m.Content, "<@"+config.BotID+">")
		message = strings.TrimSpace(message)
		messageSlice := strings.Split(message, " ")
		if m.Author.ID == config.Admin || m.Author.ID == serverOwner {
			if len(messageSlice) > 2 && strings.EqualFold(messageSlice[0], "set") {

				if strings.EqualFold(messageSlice[1], "twitchChannel") {
					addAssociation(serverID, strings.ToLower(messageSlice[2]))
					fmt.Println("Associating " + server.Name + " to the twitch channel " + messageSlice[2])
				}

			}
		}
		return

	}

	//break up the message into a slice
	message := strings.TrimSpace(m.Content)
	messageSlice := strings.Split(message, " ")

	//get the channel config from the map, and see if it exists
	channelconfig, exists := channels[config.Associations[serverID]]

	//if it doesnt, check if any integrations are in the channel
	if !exists {
		integrations, _ := s.GuildIntegrations(serverID)
		if len(integrations) > 0 {
			twitchuser := integrations[0].Account
			twitchusername := twitchuser.Name
			addAssociation(serverID, strings.ToLower(twitchusername))
		}
		return
	}

	//isolate the prefix and the first word, for convenience
	prefix := channelconfig.CommandPrefix
	//if the message doesn't start with a prefix, dont bother checking anything else
	if !strings.HasPrefix(messageSlice[0], prefix) {
		return
	}
	first := strings.TrimPrefix(messageSlice[0], prefix)

	//!commands
	if strings.EqualFold(first, "commands") {

		send("You can find the list of commands at "+config.WebsiteRoot+"c/"+config.Associations[serverID]+"/#commands", channel, nil, s, m)
	}
	//!quotes
	if strings.EqualFold(first, "quotes") {
		send("You can find the list of quotes at "+config.WebsiteRoot+"c/"+config.Associations[serverID]+"/#quotes", channel, nil, s, m)
	}

	//!roll
	if strings.EqualFold(first, "roll") && (channelconfig.RollLevel == "everyone" || channelconfig.RollLevel == "regulars") {
		s1 := rand.NewSource(time.Now().UnixNano())
		r1 := rand.New(s1)
		if len(messageSlice) > 1 {
			match, _ := regexp.MatchString("[123456]d\\d+", messageSlice[1])
			if randMax, err := strconv.Atoi(messageSlice[1]); err == nil {

				num := r1.Intn(randMax)
				num++
				send(m.Author.Username+" rolled: "+fmt.Sprint(num)+".", channel, nil, s, m)
			} else if match {

				diceNum, _ := strconv.Atoi(messageSlice[1][:1])
				diceMax, _ := strconv.Atoi(messageSlice[1][2:])

				diceStr := m.Author.Username + " rolled: "
				for i := 0; i < diceNum; i++ {
					randNum := r1.Intn(diceMax) + 1
					diceStr += fmt.Sprint(randNum) + ", "
				}
				diceStr = diceStr[:len(diceStr)-2]
				send(diceStr+".", channel, nil, s, m)
			}
		} else {
			num := r1.Intn(20)
			num++
			send(m.Author.Username+" rolled: "+fmt.Sprint(num)+".", channel, nil, s, m)
		}
	}

	//!quote
	if len(messageSlice) > 1 && strings.EqualFold(messageSlice[0], prefix+"quote") {

		if strings.EqualFold("get", messageSlice[1]) && len(messageSlice) > 2 {

			if quoteIndex, err := strconv.Atoi(messageSlice[2]); err == nil {

				if quoteIndex <= len(channelconfig.Quotes) {
					send(channelconfig.Quotes[quoteIndex-1].Text, channel, nil, s, m)
				} else {
					send("No quote at requested index.", channel, nil, s, m)
				}
			}
		} else if quoteIndex, err := strconv.Atoi(messageSlice[1]); err == nil {

			if quoteIndex <= len(channelconfig.Quotes) && quoteIndex > 0 {
				send(channelconfig.Quotes[quoteIndex-1].Text, channel, nil, s, m)

			} else {
				send("No quote at requested index.", channel, nil, s, m)
			}

		} else if strings.EqualFold("random", messageSlice[1]) {
			s1 := rand.NewSource(time.Now().UnixNano())
			r1 := rand.New(s1)
			randIndex := r1.Intn(len(channelconfig.Quotes))
			fixedRandIndex := randIndex + 1
			quoteText := "Quote #" + fmt.Sprint(fixedRandIndex) + ": " + channelconfig.Quotes[randIndex].Text
			send(quoteText, channel, nil, s, m)
		}

	}

	//see if the first word is referencing a command
	for _, command := range channelconfig.Commands {
		if strings.EqualFold(command.Key, first) {
			send(command.Value, channel, messageSlice[1:], s, m)
			//s.ChannelMessageSend(m.ChannelID,command.Value)
		}
	}
	for k, v := range channelconfig.Lists {
		if strings.EqualFold(k, first) {
			if strings.EqualFold("random", messageSlice[1]) {
				s1 := rand.NewSource(time.Now().UnixNano())
				r1 := rand.New(s1)
				randIndex := r1.Intn(len(v.Items))
				send(v.Items[randIndex], channel, messageSlice[2:], s, m)
			} else if listIndex, err := strconv.Atoi(messageSlice[1]); err == nil {
				if listIndex <= len(v.Items) && listIndex > 0 {
					send(v.Items[listIndex-1], channel, messageSlice[2:], s, m)
				} else {
					send("List index out of range.", channel, nil, s, m)
				}
			}
		}
	}

}
func send(message string, channel *discordgo.Channel, extra []string, s *discordgo.Session, m *discordgo.MessageCreate) {
	serverID := channel.GuildID
	server, _ := s.Guild(serverID)
	//String replacements
	fused := fuse(extra)
	parameters := strings.Split(fused, ";")
	channelconfig, _ := channels[config.Associations[serverID]]

	//TODO make this work for more than the exact number of parameters

	for index, _ := range parameters {
		rep := len(parameters)
		if rep > 1 && strings.Contains(message, "(_PARAMETER_)") {
			message = strings.Replace(message, "(_PARAMETER_)", parameters[index], 1)
			break
		} else if strings.Contains(message, "(_PARAMETER_)") {
			message = strings.Replace(message, "(_PARAMETER_)", parameters[index], -1)
			break
		}
		if rep > 1 && strings.Contains(message, "(_PARAMETER_CAPS_)") {
			message = strings.Replace(message, "(_PARAMETER_CAPS_)", strings.ToUpper(parameters[index]), 1)
			break
		} else if strings.Contains(message, "(_PARAMETER_CAPS_)") {
			message = strings.Replace(message, "(_PARAMETER_CAPS_)", strings.ToUpper(parameters[index]), -1)
			break
		}
	}
	//beginning of string replacement implementation
	if strings.Contains(message, "(_USER_)") {
		message = strings.Replace(message, "(_USER_)", m.Author.Username, -1)
	}
	if strings.Contains(message, "(_CHANNEL_URL_)") {
		message = strings.Replace(message, "(_CHANNEL_URL_)", "http://twitch.tv/"+config.Associations[serverID], -1)
	}
	if strings.Contains(message, "(_QUOTE_)") {
		s1 := rand.NewSource(time.Now().UnixNano())
		r1 := rand.New(s1)
		randIndex := r1.Intn(len(channelconfig.Quotes))
		message = strings.Replace(message, "(_QUOTE_)", channelconfig.Quotes[randIndex].Text, -1)
	}
	if strings.Contains(message, "(_ONLINE_CHECK_)") {
		message = strings.Replace(message, "(_ONLINE_CHECK_)", "", -1)
	}

	if strings.Contains(message, "(_LIST_") && strings.Contains(message, "_RANDOM_)") {
		listStart := strings.Index(message, "(_LIST_") + 7
		listEnd := strings.Index(message, "_RANDOM_)")
		listName := message[listStart:listEnd]
		for k, v := range channelconfig.Lists {
			if strings.EqualFold(k, listName) {
				s1 := rand.NewSource(time.Now().UnixNano())
				r1 := rand.New(s1)
				randIndex := r1.Intn(len(v.Items))
				message = strings.Replace(message, "(_LIST_"+listName+"_RANDOM_)", v.Items[randIndex], -1)
			}
		}

	}
	if message != "" {
		fmt.Println("SEND > " + server.Name + ":" + channel.Name + ": " + message)
		s.ChannelMessageSend(channel.ID, message)

	}
}

func fuse(toFuse []string) (fused string) {
	fused = ""
	for _, element := range toFuse {
		fused += element + " "
	}
	fused = strings.TrimSpace(fused)
	return
}
